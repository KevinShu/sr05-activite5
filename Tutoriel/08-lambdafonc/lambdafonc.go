package main

import (
    "fmt"
    "time"
)

func main () {
    fmt.Print("\n Hello World!\n") 
	go func(s int) {
        fmt.Println(s)
        time.Sleep(100 * time.Second)
        fmt.Print("\n Hello123\n") 
    }(2000)
    go func(s int) {
        fmt.Println(s)
        time.Sleep(100 * time.Second)
        fmt.Print("\n Hello123\n") 
    }(1000)
    fmt.Print("\n Hell=d!\n") 
}