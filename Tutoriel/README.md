# SR05 Activité 5 - Go et go routines

## Tuto

### 01
```
shubq@Kevins-MacBook-Air 01-hello % go build hello.go
shubq@Kevins-MacBook-Air 01-hello % ./hello

 Hello World!
```

### 02
```
shubq@Kevins-MacBook-Air 02-commandline % go build commandline.go 
shubq@Kevins-MacBook-Air 02-commandline % ./commandline          

 nombre fourni = 53
shubq@Kevins-MacBook-Air 02-commandline % ./commandline -h    
Usage of ./commandline:
  -n int
        nombre (default 53)
shubq@Kevins-MacBook-Air 02-commandline % ./commandline -n 875

 nombre fourni = 875
```

### 03
```
shubq@Kevins-MacBook-Air 03-tableau % go build tableau.go
shubq@Kevins-MacBook-Air 03-tableau % ./tableau 

 tabnum[0] = 0 (zéro)
 tabnum[1] = 1 (un)
 tabnum[2] = 2 (deux)
 tabnum[3] = 3 (trois)
 tabnum[4] = 4 (quatre)
 tabnum[5] = 5 (cinq)
shubq@Kevins-MacBook-Air 03-tableau % ./tableau -h
Usage of ./tableau:
  -n int
        nombre (default 5)
shubq@Kevins-MacBook-Air 03-tableau % ./tableau -n 20
 tabnum[0] = 0 (zéro)
 tabnum[1] = 1 (un)
 tabnum[2] = 2 (deux)
 tabnum[3] = 3 (trois)
 tabnum[4] = 4 (quatre)
 tabnum[5] = 5 (cinq)
 tabnum[6] = 6
 tabnum[7] = 7
 tabnum[8] = 8
 tabnum[9] = 9
 tabnum[10] = 10
 tabnum[11] = 11
 tabnum[12] = 12
 tabnum[13] = 13
 tabnum[14] = 14
 tabnum[15] = 15
 tabnum[16] = 16
 tabnum[17] = 17
 tabnum[18] = 18
 tabnum[19] = 19
 tabnum[20] = 20
```

### 04
```
shubq@Kevins-MacBook-Air 04-fonction % go build fonction.go 
shubq@Kevins-MacBook-Air 04-fonction % ./fonction

 tabnum[0] = 0 (zéro)
 tabnum[1] = 1 (un)
 tabnum[2] = 2 (deux)
 tabnum[3] = 3 (trois)
 tabnum[4] = 4 (quatre)
 tabnum[5] = 5 (cinq)
 tabnum[6] = 6
 tabnum[7] = 7
 tabnum[8] = 8
 tabnum[9] = 9
 tabnum[10] = 10
Dans main(), tabnum =[0 1 2 3 4 5 6 7 8 9 10]
Dans afficher(), tabint =[4 5 6 7]
 tabint[0] = 4
 tabint[1] = 5
 tabint[2] = 6
 tabint[3] = 7
shubq@Kevins-MacBook-Air 04-fonction % ./fonction -h
Usage of ./fonction:
  -n int
        nombre (default 10)
shubq@Kevins-MacBook-Air 04-fonction % ./fonction -n 15

 tabnum[0] = 0 (zéro)
 tabnum[1] = 1 (un)
 tabnum[2] = 2 (deux)
 tabnum[3] = 3 (trois)
 tabnum[4] = 4 (quatre)
 tabnum[5] = 5 (cinq)
 tabnum[6] = 6
 tabnum[7] = 7
 tabnum[8] = 8
 tabnum[9] = 9
 tabnum[10] = 10
 tabnum[11] = 11
 tabnum[12] = 12
 tabnum[13] = 13
 tabnum[14] = 14
 tabnum[15] = 15
Dans main(), tabnum =[0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15]
Dans afficher(), tabint =[4 5 6 7]
 tabint[0] = 4
 tabint[1] = 5
 tabint[2] = 6
 tabint[3] = 7
```

### 05 - 1
```
shubq@Kevins-MacBook-Air 05-routine % go build routine.go 
shubq@Kevins-MacBook-Air 05-routine % ./routine

 Début du long travail 1
 Début du long travail 2
 Début du long travail 0
 Début du long travail 3
 Début du long travail 4
 Fin du long travail 4
 Fin du long travail 2
 Fin du long travail 1
 Fin du long travail 3
 Fin du long travail 0
```

### 05 - 2
```
shubq@Kevins-MacBook-Air 05-routine % go build routine2.go
shubq@Kevins-MacBook-Air 05-routine % ./routine2          

 Début du long travail 5
 Début du long travail 5
 Début du long travail 5
 Début du long travail 5
 Début du long travail 5
 Fin du long travail 5
 Fin du long travail 5
 Fin du long travail 5
 Fin du long travail 5
 Fin du long travail 5
```


### 06
```
shubq@Kevins-MacBook-Air 06-routifonc % go build routfonc.go 
shubq@Kevins-MacBook-Air 06-routifonc % ./routfonc 

   Début du long travail plus 0
 Début du long travail 0
 Début du long travail 3
   Début du long travail plus 3
 Début du long travail 1
 Début du long travail 4
 Début du long travail 2
   Début du long travail plus 4
   Début du long travail plus 1
   Début du long travail plus 2
   Fin du long travail plus 0
   Fin du long travail plus 2
 Fin du long travail 4
   Fin du long travail plus 3
   Fin du long travail plus 4
 Fin du long travail 1
 Fin du long travail 2
 Fin du long travail 0
 Fin du long travail 3
   Fin du long travail plus 1 
```

### 07
```
shubq@Kevins-MacBook-Air 07-routichan % go build routichan.go 
shubq@Kevins-MacBook-Air 07-routichan % ./routichan 

 Début du long travail 0
   Attente fin du long travail 0
 Début du long travail 2
 Début du long travail 4
   Attente fin du long travail 4
   Attente fin du long travail 2
 Début du long travail 1
   Attente fin du long travail 3
 Début du long travail 3
   Attente fin du long travail 1
 Fin du long travail 4
 Fin du long travail 1
   Début du long travail bis 1
 Fin du long travail 3
   Début du long travail bis 3
   Début du long travail bis 4
 Fin du long travail 0
   Début du long travail bis 0
 Fin du long travail 2
   Début du long travail bis 2
   Fin du long travail bis 0
   Fin du long travail bis 4
   Fin du long travail bis 3
   Fin du long travail bis 1
   Fin du long travail bis 2
```