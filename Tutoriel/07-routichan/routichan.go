package main

import (
    "fmt"
    "time"
)

// ecrivain
func travailler(k int, c chan<- int) {
    fmt.Print("\n Début du long travail ", k)
    time.Sleep(1 * time.Second)
    fmt.Print("\n Fin du long travail ", k)
	// ecriture c <- msg
    c <- k
}

// lecteur
func travailler_plus(k int, c <-chan int) {
    fmt.Print("\n   Attente fin du long travail ", k)
	// lecture res = <- c : lecture de msg et stocker dans le variable res
	// lecture <- c : on veut juste la sync, on s'interesse pas a la contenu du msg
    <- c
    fmt.Print("\n   Début du long travail bis ", k)
    time.Sleep(1 * time.Second)
    fmt.Print("\n   Fin du long travail bis ", k)
}

func main () {

    var tabchan[5] chan int;

    for i := 0 ; i<5 ; i++ {
        tabchan[i] = make(chan int)
        go travailler(i, tabchan[i])
        go travailler_plus(i, tabchan[i])
    }

    fmt.Scanln() // Pour attendre la fin des goroutines...    
}