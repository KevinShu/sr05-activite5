package main

import (
    "fmt"
    "time"
)

func main () {

    for i := 0 ; i<5 ; i++ {
		// ATTENTION : ici, on sait pas exactement la valeur i quand l'execution
		// Il est fortement possible que la valeur i arrive deja a 5 quand tous les goroutines prendrait la main sur l'exe
        go func() {
            fmt.Print("\n Début du long travail ", i)
            time.Sleep(1 * time.Second)
            fmt.Print("\n Fin du long travail ", i)
        }()
    }

    // Boucle infinie peu gourmande pour attendre la fin des go-routines
    for {
        time.Sleep(time.Duration(60) * time.Second)
    }
}