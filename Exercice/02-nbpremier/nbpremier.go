package main

import (
	"fmt"
	"flag"
)

func filtrer(i int, inChan <-chan int) {
	fmt.Printf("%d est un nb premier \n", i)
	// création d'un channel pour écrire les nombres filtré
	outChan := make(chan int)

	// goroutine pour filtrer les multiples de i
	go func() {
		// lecture: pour chaque nombre qui vient du channel
		for n := range inChan {
			// supprime (ignore) tous les multiples de i.
			if n%i != 0 {
				// si le nombre ne peut pas diviser par i, on l'écrit dans le out channel
				outChan <- n
			}
		}
		// on ferme le channel après que tous les nombre passant le filtre a été écrit
		close(outChan)
	}()

	// outChan contient des nombres qui ont passé le filtre
	// on lit d'abord le premier nombre dans ce channel
	// les autres sont passés en paramètre dans filtrer() pour le filtre prochain
	for n := range outChan {
		// S'il n'y a pas de filtre suivant, un filtre n est créé.
		filtrer(n, outChan)
	}
}

func main () {

	// récupérer le nombre maximal
	max_input := flag.Int("n", 100, "maximum")
	flag.Parse()

	max := *max_input

	inChan := make(chan int)

	go func() {
		for i := 2; i <= max; i++ {
			// envoyer tous les nombre jusqu'à max au channel
			inChan <- i
		}
		// fermer le channel après toutes écritures
		close(inChan)
	}()

	// lancer le tout premier filtre : filtre du nombre 2
	filtrer(2, inChan)

}