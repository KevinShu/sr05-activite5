package main

import (
	"fmt"
	"flag"
)

func filtrer(i int, inChan <-chan int, outChan chan<- int, resultChan chan<- int) {
	for n := range inChan {
		if n%i != 0 {
			outChan <- n
		} else if n == 0 {
			resultChan <- i
			break
		}
	}
	close(outChan)
}

func main() {
	
	// récupérer le nombre maximal
	max_input := flag.Int("n", 100, "maximum")
	flag.Parse()

	max := *max_input

	inChan := make(chan int)
	outChan := make(chan int)
	resultChan := make(chan int)

	go func() {
		for i := 2; i <= max; i++ {
			inChan <- i
		}
		inChan <- 0 // Valeur particulière pour récupérer les résultats
		close(inChan)
	}()

	go filtrer(2, inChan, outChan, resultChan)

	primes := []int{2}
	for n := range outChan {
		if n == 0 {
			break
		}
		primes = append(primes, n)
		go filtrer(n, outChan, make(chan int), resultChan)
	}

	goRoutineNumbers := []int{}
	for i := 0; i < len(primes); i++ {
		goRoutineNumbers = append(goRoutineNumbers, <-resultChan)
	}

	fmt.Printf("Liste des nombres premiers jusqu'à %d : %v\n", max, primes)
	fmt.Printf("Numéros des go-routines reçus, dans le bon ordre : %v\n", goRoutineNumbers)
}