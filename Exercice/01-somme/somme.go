package main

import (
    "fmt"
	"flag"
	"runtime"
)

func sommer(tab []int, tunnel chan int) {
	var somme int
	for i:=0 ; i<len(tab); i++ {
		somme += tab[i]
	}
	tunnel <- somme
}

func main () {

	// initialisation du tableau de nombre à sommer
	taille_tab := flag.Int("n", 8, "taille du taleau")
	flag.Parse()

	tab_nombre := make([]int, *taille_tab)
	fmt.Println("Generation d'un tableau tab de taille ", *taille_tab)
    for i := 0 ; i < *taille_tab ; i++ {
        tab_nombre[i] = i;
    }
	fmt.Println("tab à sommer = ", tab_nombre);

	// récupérer le nombre de processus
	var nbcpu = runtime.NumCPU()
	fmt.Println("nombre de cpu = ", nbcpu);

	// calculer le taille de tableau que chaque goroutine doit travailler
	division := *taille_tab / nbcpu
	rest := *taille_tab % nbcpu

	// initialisation de channel
	resultChan := make(chan int, nbcpu)

	// lancer les goroutine pour sommer chaque sous-tableau
	start := 0
	end := 0
	for i := 0 ; i < nbcpu ; i++ {
		if (i < rest){
			end = start + division + 1
		} else {
			end = start + division
		}
		if i == nbcpu-1 {
			end = *taille_tab
		}
		fmt.Println("sous tableau", i, "=", tab_nombre[start : end]);
        go sommer(tab_nombre[start : end], resultChan)
		start = end
    }

	somme := 0
	for i := 0 ; i < nbcpu ; i++ {
		sommePartiel := <-resultChan
		somme += sommePartiel
		fmt.Println("résult de", i, "goroutine =", sommePartiel);  
    }

    fmt.Println("résult final =", somme);  
}