# SR05 Activité 5 - Go et go routines

## Exercice

### 1. Programme parallèle pour sommer les valeurs d'un tableau

Commandes:
```zsh
shubq@Kevins-MacBook-Air 01-somme % go build somme.go
shubq@Kevins-MacBook-Air 01-somme % ./somme
```
Résultat:
```
Generation d'un tableau tab de taille  8
tab à sommer =  [0 1 2 3 4 5 6 7]
nombre de cpu =  8
sous tableau 0 = [0]
sous tableau 1 = [1]
sous tableau 2 = [2]
sous tableau 3 = [3]
sous tableau 4 = [4]
sous tableau 5 = [5]
sous tableau 6 = [6]
sous tableau 7 = [7]
résult de 0 goroutine = 0
résult de 1 goroutine = 7
résult de 2 goroutine = 3
résult de 3 goroutine = 1
résult de 4 goroutine = 2
résult de 5 goroutine = 5
résult de 6 goroutine = 4
résult de 7 goroutine = 6
résult final = 28
```

Commandes:
```bash
shubq@Kevins-MacBook-Air 01-somme % ./somme -n 16
```

Résultat:
```
Generation d'un tableau tab de taille  16
tab à sommer =  [0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15]
nombre de cpu =  8
sous tableau 0 = [0 1]
sous tableau 1 = [2 3]
sous tableau 2 = [4 5]
sous tableau 3 = [6 7]
sous tableau 4 = [8 9]
sous tableau 5 = [10 11]
sous tableau 6 = [12 13]
sous tableau 7 = [14 15]
résult de 0 goroutine = 5
résult de 1 goroutine = 29
résult de 2 goroutine = 1
résult de 3 goroutine = 9
résult de 4 goroutine = 21
résult de 5 goroutine = 13
résult de 6 goroutine = 25
résult de 7 goroutine = 17
résult final = 120
```

Commandes:
```zsh
shubq@Kevins-MacBook-Air 01-somme % ./somme -n 17
```

Résultat:
```
Generation d'un tableau tab de taille  17
tab à sommer =  [0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16]
nombre de cpu =  8
sous tableau 0 = [0 1 2]
sous tableau 1 = [3 4]
sous tableau 2 = [5 6]
sous tableau 3 = [7 8]
sous tableau 4 = [9 10]
sous tableau 5 = [11 12]
sous tableau 6 = [13 14]
sous tableau 7 = [15 16]
résult de 0 goroutine = 3
résult de 1 goroutine = 15
résult de 2 goroutine = 31
résult de 3 goroutine = 11
résult de 4 goroutine = 23
résult de 5 goroutine = 19
résult de 6 goroutine = 27
résult de 7 goroutine = 7
résult final = 136
```

Commandes:
```zsh
shubq@Kevins-MacBook-Air 01-somme % ./somme -n 20
```

Résultat:
```
Generation d'un tableau tab de taille  20
tab à sommer =  [0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19]
nombre de cpu =  8
sous tableau 0 = [0 1 2]
sous tableau 1 = [3 4 5]
sous tableau 2 = [6 7 8]
sous tableau 3 = [9 10 11]
sous tableau 4 = [12 13]
sous tableau 5 = [14 15]
sous tableau 6 = [16 17]
sous tableau 7 = [18 19]
résult de 0 goroutine = 37
résult de 1 goroutine = 30
résult de 2 goroutine = 3
résult de 3 goroutine = 12
résult de 4 goroutine = 21
résult de 5 goroutine = 29
résult de 6 goroutine = 25
résult de 7 goroutine = 33
résult final = 190
```


### 2. Crible de Hoare pour la recherche des nombres premiers

Commandes:
```zsh
shubq@Kevins-MacBook-Air 02-nbpremier % go build nbpremier.go
shubq@Kevins-MacBook-Air 02-nbpremier % ./nbpremier   
```

Résultat:
```
2 est un nb premier 
3 est un nb premier 
5 est un nb premier 
7 est un nb premier 
11 est un nb premier 
13 est un nb premier 
17 est un nb premier 
19 est un nb premier 
23 est un nb premier 
29 est un nb premier 
31 est un nb premier 
37 est un nb premier 
41 est un nb premier 
43 est un nb premier 
47 est un nb premier 
53 est un nb premier 
59 est un nb premier 
61 est un nb premier 
67 est un nb premier 
71 est un nb premier 
73 est un nb premier 
79 est un nb premier 
83 est un nb premier 
89 est un nb premier 
97 est un nb premier 
```

Commandes:
```zsh
shubq@Kevins-MacBook-Air 02-nbpremier % ./nbpremier -n 20
```

Résultat:
```
2 est un nb premier 
3 est un nb premier 
5 est un nb premier 
7 est un nb premier 
11 est un nb premier 
13 est un nb premier 
17 est un nb premier 
19 est un nb premier 
```