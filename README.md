# SR05 Activité 5

| **Auteurs**             |   **Contact**              |
|:------------------------| :------------- |
| Bingqian Shu            |  bingqian.shu@etu.utc.fr |
| M. Bertrand Ducourthial |  bertrand.ducourthial@utc.fr |

## [Partie 1 Tutoriel](https://gitlab.com/KevinShu/sr05-activite5/-/tree/main/Tutoriel)

## [Partie 2 Exercice](https://gitlab.com/KevinShu/sr05-activite5/-/tree/main/Exercice)
### [Ex Somme](https://gitlab.com/KevinShu/sr05-activite5/-/blob/main/Exercice/01-somme/somme.go)
### [Ex Nb Premier](https://gitlab.com/KevinShu/sr05-activite5/-/blob/main/Exercice/02-nbpremier/nbpremier.go)